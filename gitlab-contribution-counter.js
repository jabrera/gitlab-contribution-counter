class GitlabContributionCounter {
    config() {
        // Edit the values of the following variables according to your configuration
        this.GITLAB_DOMAIN = "https://gitlab.your-domain.com"; // no ending forward slash
        this.GITLAB_USERNAME = "your-username";
        this.GITLAB_VERSION = 15;
    }
    constructor() {
        this.config();
        this.GITLAB_VERSIONS = {
            "11": {
                "URL": `${this.GITLAB_DOMAIN}/${this.GITLAB_USERNAME}`
            },
            "15": {
                "URL": `${this.GITLAB_DOMAIN}/users/${this.GITLAB_USERNAME}/activity`
            }
        };
        this.ERROR_MESSAGE = "Invalid Gitlab version. See `GITLAB_VERSIONS` for supported versions.\nIf you don't see your version, kindly create an issue at https://gitlab.com/juvarabrera/gitlab-contribution-counter/-/issues";
        this.data = {};
    }
    getActivity(limit,offset) {
        let obj = this;
        return new Promise((resolve, reject) => {
            if(!obj.GITLAB_VERSIONS.hasOwnProperty(obj.GITLAB_VERSION))
                reject(obj.ERROR_MESSAGE);
            let url = `${obj.GITLAB_VERSIONS[obj.GITLAB_VERSION].URL}?limit=${limit}&offset=${offset}`
            console.log(`Retrieving ${url}...`)
            fetch(url, {
                headers: {
                    "Accept":  "application/json, text/plain, */*",
                    "Accept-Language": "en-US,en;q=0.9,fil;q=0.8",
                    "Referer": `${obj.GITLAB_DOMAIN}/users/${obj.GITLAB_USERNAME}/activity`,
                    "X-CSRF-Token": $("meta[name='csrf-token']").attr("content"),
                    "X-Requested-With": "XMLHttpRequest"
                }
            }).then(response => {
                if (response.status >= 200 && response.status <= 400)
                    return response.json();
                reject(obj.ERROR_MESSAGE);
            }).then(data => {
                resolve(data);
            })
        })
    }
    async init() {
        var obj = this;
        var limit = 100;
        var offset = 0;
        while(true) {
            var data = await obj.getActivity(limit, offset);
            if (data.count == 0) break;
            var jhtml = $("<div>" + data.html + "</div>");
            jhtml.find(".event-item").each((index, eventItem) => {
                var date = eventItem.querySelector("time").getAttribute("datetime");
                date = new Date(date);
                var dateString = date.getFullYear() + "-" + ((date.getMonth() + 1) < 10 ? "0" + (date.getMonth() + 1) : (date.getMonth() + 1)) + "-" + (date.getDate() < 10 ? "0" + date.getDate() : date.getDate());
                if (obj.data.hasOwnProperty(dateString)) {
                    obj.data[dateString]++;
                } else {
                    obj.data[dateString] = 1;
                }
            });
            offset += limit;
        }
        console.log("Done!");
        console.log(this.data);
    }
}

var gitlabContributionCounter = new GitlabContributionCounter();
await gitlabContributionCounter.init();

# gitlab-contribution-counter
Counts your contribution on your custom gitlab domain. If this helps you or this is what you're looking for, a ⭐ is appreciated :)

## About
Gitlab can be hosted on your own domain such as `https://gitlab.your-domain.com/`. With limited API functionality, you cannot retrieve ALL of your activities/contributions on your Gitlab domain. This is a script that gets your number of contributions per day from the start. 

## Supported Gitlab versions
Gitlab versions remove contribution history that is older than X years. The following table gives you how much data you can pull depending on the Gitlab version you are using.
| version | contribution history | supported |
| --- | --- | --- |
| [15](https://docs.gitlab.com/15.2/ee/api/events.html#event-time-period-limit) | at most 3 years | yes |
| [14](https://docs.gitlab.com/14.10/ee/api/events.html#event-time-period-limit) | at most 3 years | not tested |
| [13](https://docs.gitlab.com/13.12/ee/api/events.html#event-time-period-limit) | at most 2 years | not tested |
| 12 | unknown | not tested |
| 11 | at most 1 year | yes |

## Ways
1. Javascript - Does not use an `Access-Token`.
2. `WIP` Python - Uses an `Access-Token`

## Return
```
{
    "YYYY-MM-DD": X,
    ...
}
```

1. Date is formated in `YYYY-MM-DD` as the key. By default, your contributions are counted based in UTC timezone.
2. `X` is the number of contributions for that day. 

## How to Use
### Javascript
1. Login to your Gitlab domain. You can run the script in any page. 
2. Open Dev Tools Console.
3. Copy the contents of `gitlab-contribution-counter.js`.
4. Paste the script but **DO NOT** run it yet.
5. Edit the `GITLAB_DOMAIN` and `GITLAB_USERNAME`.
6. Edit the `GITLAB_VERSION`. See `GITLAB_VERSIONS` to see supported versions. If you're using gitlab v14, try to use v11. _Note: This has not been tested for gitlab versions 12-14._
6. Run the script.
7. Wait for it to finish. You should see the `Done!` message and the actual result in JSON.

### Python
_Work in progress_
